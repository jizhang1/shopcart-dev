package cn.cart.es.service;


import cn.cart.es.ProductRepository;
import cn.cart.vo.CartVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<CartVo> searchByName(String keyword) {
        return productRepository.findByNameContaining(keyword);
    }

    public List<CartVo> searchByDescription(String keyword) {
        return productRepository.findByDescriptionContaining(keyword);
    }



    public CartVo save(CartVo cartVo){
        return productRepository.save(cartVo);
    }


    // 其他搜索方法
}
