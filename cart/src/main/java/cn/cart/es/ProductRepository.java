package cn.cart.es;


import cn.cart.vo.CartVo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends ElasticsearchRepository<CartVo, String> {

    List<CartVo> findByNameContaining(String keyword);

    List<CartVo> findByDescriptionContaining(String keyword);

    // 其他自定义查询方法


}
