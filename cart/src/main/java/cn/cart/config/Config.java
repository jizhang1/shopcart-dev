//package cn.cart.config;
//
//import co.elastic.clients.elasticsearch.nodes.Client;
//import org.elasticsearch.client.transport.TransportClient;
//import org.elasticsearch.common.transport.TransportAddress;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class Config {
//    @Bean
//    public Client client() {
//        TransportClient client = new TransportClient();
//        TransportAddress address = new InetSocketTransportAddress(
//                "localhost",9300);
//        client.addTransportAddress(address);
//        return client;
//    }
//}
