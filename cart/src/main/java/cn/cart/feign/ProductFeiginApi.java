package cn.cart.feign;


import cn.cart.domain.test.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

@Component
@FeignClient(name = "product-service")
public interface ProductFeiginApi {

    @RequestMapping("/product/getById")
    public Product queryById(Long id);
}
