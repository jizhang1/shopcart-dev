package cn.cart.vo;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "products")
@Data
public class CartVo {
    @Id
    private Long id;//购物车id
    private Long userId;
    private Long productId;
    private Long price;//商品的总价格
    @Field(type = FieldType.Keyword)
    private String name;//商品名称
    private Long quantity;//商品数量
    private String merchantName;//商家名称
    private String description;


}
