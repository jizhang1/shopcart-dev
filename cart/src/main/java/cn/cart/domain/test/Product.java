package cn.cart.domain.test;

import lombok.Data;

@Data
public class Product {
    private Long id;
    private String name;//商品名称
    private String description;
    private Long price;//商品价格
    private Long merchantId;//商家Id
    private String merchantName;//商家名称
    private Long stock;//商品库存

}
