package cn.cart.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Getter
@Setter

public class EsProduct {

    private String id;
    private String name;
    private String description;


    // Getters and setters
}
