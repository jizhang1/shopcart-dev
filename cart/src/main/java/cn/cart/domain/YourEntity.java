package cn.cart.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Getter
@Setter

public class YourEntity {

    private String id;
    private String field1;
    private String field2;

    // 构造方法、getter和setter省略
}
