package cn.cart.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;



@Data
@TableName("shop_cart")
public class ShopCart {
    @TableId(type = IdType.AUTO)
    private Long id;

    private Long userId;//用户id

    private Long productId;//商品id

    private Long merchantId;//商家

    private Long quantity;//商品数量



}
