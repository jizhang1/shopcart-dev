package cn.cart.mapper;


import cn.cart.domain.ShopCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;



@Mapper
public interface CartMapper extends BaseMapper<ShopCart> {
}
