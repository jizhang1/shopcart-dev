package cn.cart.controller;



import cn.cart.domain.ShopCart;
import cn.cart.domain.test.Product;


import cn.cart.es.ProductRepository;
import cn.cart.es.service.ProductService;
import cn.cart.mapper.CartMapper;
import cn.cart.vo.CartVo;
import jakarta.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

@RestController
@RequestMapping("/cart")
public class CartController {
//    @Autowired
//    ProductService productService;
//    @Autowired
//    MyDocumentRepository myDocumentRepository;

    @Autowired
    ProductService productService;
    @Autowired
    ProductRepository repository;
    @Autowired
    CartMapper cartMapper;
//    @Autowired
//    ProductFeiginApi productFeiginApi;
    @Autowired
    private StringRedisTemplate redisTemplate;
    /**
     * 添加购物车
     * @param productId 商品id
     * @param request
     */
    @RequestMapping("/add")
    public String addCart (Long productId,Long quantity, HttpServletRequest request){
//        String token = request.getHeader("userToken");//网关发送来的token
//        String userId = redisTemplate.opsForValue().get("userToken");//获取用户的id信息
//        userId= JSON.parseObject(userId,String.class);
//
//
//        //远程调用商品服务,通过productId获取商品的信息
//        Product product = productService.queryById(productId);
//
//        if (product.getStock()<quantity)//判断添加进去的商量是否大于库存
//        {
//            //抛出异常
//            return "添加失败";
//        }


        //下面是测试数据
        Long userId=1L;
        Product product = new Product();
        product.setDescription("好喝");
        product.setPrice(99l);
        product.setName("可乐");
        product.setStock(300l);
        product.setMerchantId(1l);
        product.setId(productId);
        product.setMerchantName("可口公司");


        //整合商品id和用户和用户信息
        ShopCart shopCart= new ShopCart();
        shopCart.setMerchantId(product.getMerchantId());
        shopCart.setQuantity(quantity);
        shopCart.setUserId(Long.valueOf(userId));
        shopCart.setProductId(productId);
        //插入数据库中
        cartMapper.insert(shopCart);
        //同时将购物车信息添加进es中
        CartVo cartVo = new CartVo();
        BeanUtils.copyProperties(shopCart,cartVo);
        cartVo.setMerchantName(product.getMerchantName());//设置商家名称
        cartVo.setPrice(quantity*product.getPrice());//设置商品总价
        cartVo.setName(product.getName());//设置商品名称
        cartVo.setDescription(product.getDescription());//设置商品描述
        CartVo save = repository.save(cartVo);
        if (save != null)
        {
            System.out.println("添加文档成功");
        }

        return "添加成功";
    }

    /**
     * 搜索功能(模糊查询)
     * @param keyword 搜索关键词
     * @return
     */
    @RequestMapping("/search")
    public List<CartVo> search(String keyword){

        List<CartVo> cartVos = productService.searchByName(keyword);

//        for (CartVo cartVo : cartVos) {
//            System.out.println(cartVo.toString());
//        }
//        if (cartVos.size()==0)
//        {cartVos =productService.searchByDescription(keyword);}

        return cartVos;
    }
}
